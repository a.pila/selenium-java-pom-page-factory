package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SearchPage {

    @FindBy(id = "search_query_top")
    public WebElement objSearchQuery;

    @FindBy(id = "submit_search")
    public WebElement objSubmitSearch;

    
}
