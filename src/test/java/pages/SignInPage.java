package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import static org.testng.AssertJUnit.assertEquals;

public class SignInPage {

    @FindBy(xpath = "//a[contains(text(),'Sign in')]")
    public WebElement objSignIn;

    public void assertTextPresence() {
        String actualMessage = objSignIn.getText();
        assertEquals(actualMessage, "Sign in");
    }
}
