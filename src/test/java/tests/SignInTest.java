package tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;
import pages.SignInPage;

public class SignInTest {

    @Test(description = "This test uss Page Factory")
    public void signInTest() {

        System.setProperty("webdriver.chrome.driver", "src/main/resources/drivers/chromedriver");

        WebDriver driver = new ChromeDriver();

        driver.navigate().to("http://automationpractice.com/index.php");

        SignInPage objSignIn = PageFactory.initElements(driver, SignInPage.class);

        objSignIn.assertTextPresence();
    }

}
