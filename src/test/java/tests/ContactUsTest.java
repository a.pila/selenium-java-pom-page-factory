package tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;
import pages.ContactUsPage;

public class ContactUsTest {

    @Test(description = "Dropdowns")
    public void ContactUsTest() {

        System.setProperty("webdriver.chrome.driver", "src/main/resources/drivers/chromedriver");

        WebDriver driver = new ChromeDriver();

        driver.navigate().to("http://automationpractice.com/index.php?controller=contact");

        ContactUsPage objContactUsPage = PageFactory.initElements(driver, ContactUsPage.class);

        Select drp = new Select(objContactUsPage.objSubject);
        drp.selectByIndex(1);

    }
}
