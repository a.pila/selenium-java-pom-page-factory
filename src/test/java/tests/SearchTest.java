package tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class SearchTest {

    @Test(description = "This test does not use Page Factory")
    public void loginTest() {

        System.setProperty("webdriver.chrome.driver", "src/main/resources/drivers/chromedriver");

        WebDriver driver = new ChromeDriver();

        driver.navigate().to("http://automationpractice.com/index.php");

        driver.findElement(By.id("search_query_top")).sendKeys("dress");

        driver.findElement(By.name("submit_search")).click();

        driver.quit();

    }

}

